#!/bin/bash
#SBATCH --account=def-asarkar
#SBATCH --gres=gpu:lgpu:4        # request GPU "generic resource"
#SBATCH --cpus-per-task=24   # maximum CPU cores per GPU request: 6 on Cedar, 16 on Graham.
#SBATCH --mem=32000M        # memory per node
#SBATCH --time=10:00      # time (DD-HH:MM)
#SBATCH --output=%N-%j.out  # %N for node name, %j for jobID

module load cuda cudnn python/3.5
source tensorflow/bin/activate
python label_image.py /PATH/TO/IMAGE/