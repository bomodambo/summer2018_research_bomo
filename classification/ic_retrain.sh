#!/bin/bash
#SBATCH --account=def-asarkar
#SBATCH --gres=gpu:lgpu:4        # request GPU "generic resource"
#SBATCH --cpus-per-task=24   # maximum CPU cores per GPU request: 6 on Cedar, 16 on Graham.
#SBATCH --mem=32000M        # memory per node
#SBATCH --time=10:00      # time (DD-HH:MM)
#SBATCH --output=%N-%j.out  # %N for node name, %j for jobID

module load cuda cudnn python/3.5
source tensorflow/bin/activate
IMAGE_SIZE=224
ARCHITECTURE="mobilenet_0.50_${IMAGE_SIZE}"
tensorboard --logdir tf_files/training_summaries &

python tensorflow-for-poets-2/scripts/retrain.py \
  --bottleneck_dir=/home/bomo/tensorflow-for-poets-2/tf_files/bottlenecks/ \
  --how_many_training_steps=500 \
  --model_dir=/home/bomo/tensorflow-for-poets-2/tf_files/models/ \
  --summaries_dir= /home/bomo/tensorflow-for-poets-2/tf_files/training summaries/ "${ARCHITECTURE}" \
  --output_graph=/home/bomo/tensorflow-for-poets-2/tf_files/retrained_graph.pb \
  --output_labels=/home/bomo/tensorflow-for-poets-2/tf_files/retrained_labels.txt \
  --architecture="${ARCHITECTURE}" \
  --image_dir=/home/bomo/tensorflow-for-poets-2/tf_files/flower_photos/
